let rightContainer = document.getElementById('contenedor-derecho');
let leftContainer = document.getElementById('contenedor-izquierdo');
drag(document.getElementById('plaza1'));
drag(document.getElementById('plaza2'));
drag(document.getElementById('plaza3'));
drag(document.getElementById('plaza4'));
drag(document.getElementById('plaza5'));
drag(document.getElementById('plaza6'));
drag(document.getElementById('plaza7'));
drag(document.getElementById('plaza8'));
drag(document.getElementById('plaza9'));
drag(document.getElementById('plaza10'));
drag(document.getElementById('plaza11'));
drag(document.getElementById('plaza12'));
drag(document.getElementById('plaza13'));
drag(document.getElementById('plaza14'));
drag(document.getElementById('plaza15'));
drag(document.getElementById('plaza16'));
drag(document.getElementById('plaza17'));
drag(document.getElementById('plaza18'));
function drag(element){
    let Dx=0;
    let Dy=0;
    element.onpointerdown=initDrag;
    function initDrag(e){
        e.preventDefault();
        document.onpointermove=dragElement;
        document.onpointerup=stopDrag;
        Dx=e.clientX- element.offsetLeft;
        Dy=e.clientY- element.offsetTop;
        document.onpointermove=dragElement;
        document.onpointerup=stopDrag;
    }
    function dragElement(e){
        element.style.left=e.clientX-Dx+'px';
        element.style.top=e.clientY-Dy+'px';
    }
    function stopDrag(){
        document.onpointermove=null;
        document.onpointerup=null;
    }
}